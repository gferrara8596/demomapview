//
//  ViewController.swift
//  DemoMapView
//
//  Created by Giuseppe Ferrara on 23/01/2020.
//  Copyright © 2020 Giuseppe Ferrara. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class ViewController: UIViewController, CLLocationManagerDelegate {

    @IBOutlet weak var MapView: MKMapView!
    let locationManager = CLLocationManager()
    
    let demoFountain = CLLocation(latitude: 37.3305, longitude: -122.0296)
    let demoBench = CLLocation(latitude: 37.330, longitude: -122.0286)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
        let _ = Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: {_ in
            self.showLocation()
        })
        
    }
    
    func showLocation(){
        MapView.setCenter(locationManager.location!.coordinate, animated: true)
        let fountainAnnotation = MKPointAnnotation()
        fountainAnnotation.coordinate = demoFountain.coordinate
        MapView.addAnnotation(fountainAnnotation)
//        let benchAnnotation = MKPointAnnotation()
//        benchAnnotation.coordinate = demoBench.coordinate
//        MapView.addAnnotation(benchAnnotation)
        let distance = locationManager.location?.distance(from: CLLocation(latitude: fountainAnnotation.coordinate.latitude,
                                                                           longitude: fountainAnnotation.coordinate.longitude))
        let region = MKCoordinateRegion(center: locationManager.location!.coordinate,
                                        latitudinalMeters: distance! + 400,
                                        longitudinalMeters: distance! + 400)
        MapView.setRegion(region, animated: true)
        MapView.showsUserLocation = true
        print("showing location: \(locationManager.location)")
        
    }
    
    
    

}

